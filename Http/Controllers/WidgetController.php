<?php namespace YCMS\Widget\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class WidgetController extends Controller {
	
	public function index()
	{
		return view('widget::index');
	}
	
}