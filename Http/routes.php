<?php

Route::group(['prefix' => 'widget', 'namespace' => 'YCMS\Widget\Http\Controllers'], function()
{
	Route::get('/', 'WidgetController@index');
});